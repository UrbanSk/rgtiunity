﻿using UnityEngine;
using System.Collections;

public class HandleAmmoBar : MonoBehaviour {
	public float maxWidth; 
	public float height; 
	// Use this for initialization
	void Start () {
		maxWidth = this.GetComponent<RectTransform>().rect.width;
		height = this.GetComponent<RectTransform>().rect.height;
	}
	
	// Update is called once per frame
	void Update () {
		int currAmmo = GameObject.FindWithTag("PlayerTank").GetComponent<Shoot>().currentWeapon.currAmmo;
		int maxAmmo = GameObject.FindWithTag("PlayerTank").GetComponent<Shoot>().currentWeapon.maxAmmo;
		this.GetComponent<RectTransform>().sizeDelta = new Vector2(maxWidth * ((float)currAmmo / maxAmmo), height);	
	}
}
