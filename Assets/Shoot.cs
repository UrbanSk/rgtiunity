﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class Shoot : MonoBehaviour {

	

	public int ammoLeft;
	public GameObject bullet;
	public float maxCooldown=0.3f;
	public float shootCooldown;
	public bool canShoot;
	public Weapon currentWeapon;
	public Weapon defaultWeapon;
	public Weapon machineGun;
	public AudioClip pistolSound;
	public AudioClip machineGunSound;

	// Use this forinitialization
	void Start () {
		defaultWeapon = new Weapon(99999, 0.75f, SpawnBullet, "Cannon", pistolSound);
		machineGun = new Weapon (50, 0.2f, SpawnBullet, "Rapid cannon", pistolSound);
		currentWeapon = defaultWeapon;
	}
	
	// Update is called once per frame
	void Update () {
		if (shootCooldown > 0) {
			this.GetComponent<Animator> ().SetBool ("IsShooting", false);
			shootCooldown-=Time.deltaTime;
		} else {
			canShoot=true;
		}
	}


	public void SpawnBullet(){
		if (!canShoot) {
			return;
		}
		this.GetComponent<Animator> ().SetBool ("IsShooting", true);
		if (currentWeapon.currAmmo > 0) {
			Object spawnedBullet = Instantiate (bullet, this.GetComponent<Transform> ().position, Quaternion.identity);
			((GameObject)spawnedBullet).GetComponent<MoveToPoint> ().target = mousePosition ();
		SphereCollider[] Colliders = ((GameObject)spawnedBullet).GetComponents<SphereCollider> ();
		foreach (var c in Colliders){
			if(!c.isTrigger){
				Physics.IgnoreCollision(c,this.GetComponent<BoxCollider>());
			}
		}
			Object.Destroy (spawnedBullet, 20f);
			canShoot = false;
			shootCooldown = currentWeapon.shootCooldown;
			currentWeapon.currAmmo--;
		} else {
			currentWeapon = defaultWeapon;
			currentWeapon.currAmmo = currentWeapon.maxAmmo;
		}
			GameObject.FindWithTag ("PlayerTank").audio.PlayOneShot (currentWeapon.sound, 1);
	}

	public Vector3 mousePosition(){
		/*Vector3 position =Input.mousePosition;
		float distance = Vector3.Distance (this.GetComponent<Transform> ().position, Camera.main.transform.position);
		position.z = 20f;
		position = Camera.main.ScreenToWorldPoint (position);
		Vector3 targetPosition = position - this.GetComponent<Transform> ().position;
		targetPosition*=60f;
		position += targetPosition;
		Debug.Log (position.y);
		//position.y = 0;*/

		Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
		//Točka na smeri žarka in na višini 0
		float t = -ray.origin.y / ray.direction.y;
		Vector3 position = ray.GetPoint (t);
		//Brez tega se premika samo do lokacije miške. S tem podaljšamo vektor za neko vrednost, tako da nadaljuje čez miško.
		Vector3 targetPosition = position - this.GetComponent<Transform> ().position;
		targetPosition.y = 0;
		targetPosition *= 6000f;
		position += targetPosition;

		return position;
	}
}
