﻿using UnityEngine;
using System.Collections;

public class HandleWallCollision : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		if (other.tag == "EnemyBullet" || other.tag == "PlayerBullet") {
			Destroy (other.gameObject);
		}
	}
}
