﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HighScoreManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public string HighScoresText(){
		string s = "High Scores\n\n";
		for (int i=0; i<10; i++) {
			int get = PlayerPrefs.GetInt(i+"Score");
			string name = PlayerPrefs.GetString(i+"Name");
			if(name.Equals(""))
				name="Unnamed Player";
			s+=name + ": " + get + "\n";
			Debug.Log(s);
		}
		return s;
	}

	public int insertScore(int score, string name){
		List<int> ret = ReadHighScores ();
		List<string> names = ReadNames ();
		for (int i=0; i<10; i++) {
			if(score>ret[i]){
				ret.Insert(i,score);
				names.Insert(i,name);
				WriteHighScores (ret);
				WriteNames(names);
				return i;
			}
		}
		return -1;
	}
	List<int> ReadHighScores(){
		List<int> ret = new List<int> ();
		for (int i=0; i<10; i++) {
			int get = PlayerPrefs.GetInt(i+"Score");
			ret.Add(get);
		}
		return ret;
	}

	List<string> ReadNames(){
		List<string> ret = new List<string> ();
		for (int i=0; i<10; i++) {
			string get = PlayerPrefs.GetString(i+"Name");
			ret.Add(get);
		}
		return ret;
	}

	void WriteHighScores(List<int> scores){
		for (int i=0; i<10; i++) {
			 PlayerPrefs.SetInt(i+"Score",scores[i]);
			 PlayerPrefs.Save();
			 //Debug.Log("Wrote " + PlayerPrefs.GetInt(i+"Score",scores[i]) + "Should be: " + scores[i]);
		}

	}
	void WriteNames(List<string> names){
		for (int i=0; i<10; i++) {
			PlayerPrefs.SetString(i+"Name",names[i]);
			PlayerPrefs.Save();
			//Debug.Log("Wrote " + PlayerPrefs.GetInt(i+"Score",scores[i]) + "Should be: " + scores[i]);
		}
		
	}

}
