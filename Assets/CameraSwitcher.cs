﻿using UnityEngine;
using System.Collections;

public class CameraSwitcher : MonoBehaviour {
	public Camera activeCamera;
	public Camera inactiveCamera;
	// Use this for initialization
	void Start () {
		activeCamera.enabled = true;
		inactiveCamera.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void switchCamera(){
		activeCamera.enabled = !activeCamera.enabled;
		inactiveCamera.enabled = !inactiveCamera.enabled;
	}
}
