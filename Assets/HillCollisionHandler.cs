﻿using UnityEngine;
using System.Collections;

public class HillCollisionHandler : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerStay(Collider other){
		float x = Random.Range (-30f, 30f);
		float z = Random.Range (-30f, 30f);
		if (other.tag == "HealthPickup" || other.tag == "MachineGunPickup") {
			other.gameObject.GetComponent<Transform>().position = new Vector3(x,0,z);
		}
	}
}
