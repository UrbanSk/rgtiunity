﻿using UnityEngine;
using System.Collections;

public class HandlePlayerCollision : MonoBehaviour {
	public float health=100;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if(health<=0){
			this.GetComponent<Animator>().SetBool("IsDead", true);
			//Destroy(this.gameObject);
		}
	}
	void OnTriggerEnter(Collider other){
		if (this.gameObject.tag != "PlayerTank") {
			return;
		}
		if (other.tag == "EnemyBullet") {
			Destroy (other.gameObject);
			health -= 30f;
		}
		if (other.tag == "ChaseEnemy") {
			if(other.gameObject == null)
		    {
				return;
			}
			Destroy (other.gameObject);
			health -= 50f;
		}
		if (other.tag == "ShootEnemy") {
			Destroy (other.gameObject);
			health -= 50f;
		}

	}
	void OnTriggerStay(Collider other){
		if (other.tag == "Lava") {
			health -= Time.deltaTime*10;
		}
	}
}
