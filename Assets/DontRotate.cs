﻿using UnityEngine;
using System.Collections;

public class DontRotate : MonoBehaviour {

	public Vector3 fixedRotation;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		this.GetComponent<Transform> ().rotation = Quaternion.Euler(fixedRotation);
	}
}
