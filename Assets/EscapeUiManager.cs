﻿using UnityEngine;
using System.Collections;

public class EscapeUiManager : MonoBehaviour {

	public void unpause(){
		Time.timeScale = 1;
	}

	public void mainMenu(){
		Application.LoadLevel ("MainMenu");
		unpause ();
	}

	public void quit(){
		Application.Quit ();
	}

	public void resume(){
		GameObject.FindWithTag ("EscapeMenu").SetActive (false);
		unpause ();
	}

	public void retry(){
		Application.LoadLevel (Application.loadedLevelName);
		unpause ();
	}


}
