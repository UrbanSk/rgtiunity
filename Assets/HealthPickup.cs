﻿using UnityEngine;
using System.Collections;

public class HealthPickup : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider other){
		if (this.gameObject.tag != "HealthPickup") {
			return;
		}
		if (other.tag == "TankBody") {
			if(GameObject.FindWithTag("PlayerTank").GetComponentInParent<HandlePlayerCollision>().health <=70)
			{
				GameObject.FindWithTag("PlayerTank").GetComponentInParent<HandlePlayerCollision>().health +=30;
			} 
			else 
			{
				GameObject.FindWithTag("PlayerTank").GetComponentInParent<HandlePlayerCollision>().health = 100;
			}

			Destroy(this.gameObject);
		}
	}
}
