﻿using UnityEngine;
using System.Collections;

public class HandleHealthBar : MonoBehaviour {
	public float maxWidth; 
	public float height; 
	// Use this for initialization
	void Start () {
		maxWidth = this.GetComponent<RectTransform>().rect.width;
		height = this.GetComponent<RectTransform>().rect.height;
	}
	
	// Update is called once per frame
	void Update () {
		float currHealth = GameObject.FindWithTag("PlayerTank").GetComponent<HandlePlayerCollision>().health;
		this.GetComponent<RectTransform>().sizeDelta = new Vector2(maxWidth * (currHealth / 100f), height);
	}
}