﻿using UnityEngine;
using System.Collections;

public class MoveScriptTest : MonoBehaviour {
	public Vector3 velocity = new Vector3(10,10,10);
	public Vector3 direction;
	public CharacterController controller;
	public Vector3 moveVector;
	// Use this for initialization
	void Start () {
		controller = this.GetComponent<CharacterController>();
	}
	

	// Update is called once per frame
	void Update () {
		Transform BodyTransform = GameObject.FindWithTag ("TankBody").transform;
		BoxCollider collider = this.GetComponent<BoxCollider> ();
		GameObject[] wheels = GameObject.FindGameObjectsWithTag ("Wheel");
		Transform transform = (Transform)this.GetComponent ("Transform");
		direction.Normalize ();
		moveVector = Vector3.Scale (velocity,direction);
		transform.Translate (moveVector * Time.deltaTime);
		if (direction.x != 0 || direction.z != 0) {
			if(Time.timeScale!=0)
				BodyTransform.forward = Quaternion.Euler (0, 90, 0) * direction;
		}
		//Debug.Log(moveVector.z + " " + moveVector.x);
		//controller.Move (moveVector);
	}
}
