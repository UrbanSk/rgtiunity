﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public GameObject inputField;

	public void setSettings(){
		PlayerPrefs.SetString("playerName", inputField.GetComponent<InputField>().text);
	}
	public void level1(){
		setSettings ();
		Application.LoadLevel ("Level1");
	}
	public void level2(){
		//TODO - Dodaj scene za level 2
		setSettings ();
		Application.LoadLevel ("Level2");
	}
}
