﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemySpawner : MonoBehaviour {
	public float minSpawn;
	public float maxSpawn;
	public int level;
	public float[] chaseSpawnCooldownLevels;
	public float chaseSpawnCooldown;
	public float[] shootSpawnCooldownLevels;
	public float shootSpawnCooldown;
	public int[] chaseNum = {5, 9, 12, 20, 30, 60};
	public int[] shootNum = {5, 10, 15, 30, 45, 60}; 
	public GameObject shootPrefabAddedFromEditor;
	public GameObject chasePrefabAddedFromEditor;
	public float cooldown;
	public Text levelText;
	// Use this for initialization
	void Start () {
		cooldown = 1f;
		level = 0;
		chaseSpawnCooldown = 3f;
		shootSpawnCooldown = 3.5f;
		levelText.text = "Level: 0";
	}
	
	// Update is called once per frame
	void Update () {
		if(chaseSpawnCooldown <= 0){
			spawnChaseEnemy();
			chaseSpawnCooldown = chaseSpawnCooldownLevels[level];
			chaseNum[level]--;
		} else {
			chaseSpawnCooldown -= Time.deltaTime;
		}

		if(shootSpawnCooldown <= 0){
			spawnShootEnemy();
			shootSpawnCooldown = shootSpawnCooldownLevels[level];
			shootNum[level]--;
		} else {
			shootSpawnCooldown -= Time.deltaTime;
		}

		if ((shootNum [level] <= 0) && (chaseNum [level] <= 0)) {
			startNewLevel ();
		}


	}

	void spawnChaseEnemy(){
		Vector3 spawnPosition = new Vector3 ();
		spawnPosition.x = Random.Range (minSpawn, maxSpawn);
		spawnPosition.z = Random.Range (minSpawn, maxSpawn);
		spawnPosition.y = 3f;
		if (Physics.OverlapSphere (spawnPosition, 1f).Length > 0) {
			Debug.Log ("incorrect placement");
			spawnChaseEnemy();
			return;
		}
		GameObject spawnedEnemy = (GameObject)Instantiate (chasePrefabAddedFromEditor, spawnPosition, Quaternion.identity);

	}

	void spawnShootEnemy(){
		Vector3 spawnPosition = new Vector3 ();
		spawnPosition.x = Random.Range (minSpawn, maxSpawn);
		spawnPosition.z = Random.Range (minSpawn, maxSpawn);
		if (Physics.OverlapSphere (spawnPosition, 1f).Length > 0) {
			Debug.Log ("incorrect placement");
			spawnShootEnemy();
			return;
		}
		GameObject spawnedEnemy = (GameObject)Instantiate (shootPrefabAddedFromEditor, spawnPosition, Quaternion.identity);
	}
	void startNewLevel(){
		level++;
		levelText.text = "Level: " + level;
	}
}
