﻿using UnityEngine;
using System.Collections;

public class MoveToPoint : MonoBehaviour {

	public Vector3 target;
	public float speed;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float step = speed * Time.deltaTime;

		this.GetComponent<Transform> ().position= Vector3.MoveTowards (this.GetComponent<Transform> ().position, target, step);
	}
}
