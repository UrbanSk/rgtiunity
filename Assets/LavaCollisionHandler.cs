﻿using UnityEngine;
using System.Collections;

public class LavaCollisionHandler : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerStay(Collider other){
		if (other.tag == "PlayerTank") {
			other.gameObject.GetComponent<HandlePlayerCollision>().health -=1;
		}
	}
}
