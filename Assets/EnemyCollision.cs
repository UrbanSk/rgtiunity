﻿using UnityEngine;
using System.Collections;

public class EnemyCollision : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		//k
		//Just handling collision between player bullets and enemies
		
		if (other.gameObject.tag.Contains ("Enemy") && this.gameObject.tag == "PlayerBullet") {
			other.gameObject.GetComponent<Enemy> ().health -= 10;
			Destroy (this.gameObject);
		}
	}

}
