﻿using UnityEngine;
using System.Collections;

public class ChaseEnemy : MonoBehaviour {

	public float speed;
	public GameObject player;
	NavMeshAgent agent;
	// Use this for initialization
	void Start () {
		//this.renderer.material.color = Color.red;
		speed = 5;
		player = GameObject.FindWithTag ("PlayerTank");
		agent = this.GetComponent<NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void Update () {
		/*float step = speed * Time.deltaTime;
		this.GetComponent<Transform> ().position= Vector3.MoveTowards (this.GetComponent<Transform> ().position, player.transform.position, step);*/
		agent.SetDestination (player.transform.position);
	}
}
