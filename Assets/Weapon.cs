using System;
using UnityEngine;
namespace AssemblyCSharp
{
		public delegate void ShootDelegate();
		public class Weapon
		{
			
			public int maxAmmo;
			public ShootDelegate shootFunction;
			public float shootCooldown;
			public int currAmmo;
			public string name;
			public AudioClip sound;
			
			public Weapon(int ammo, float cooldown, ShootDelegate func, string n, AudioClip audio)
			{
				maxAmmo = ammo;
				currAmmo = ammo;
				shootCooldown = cooldown;
				shootFunction = func;
				name = n;
				sound = audio;
			}
		}
}

