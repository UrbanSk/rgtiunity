﻿using UnityEngine;
using System.Collections;

public class ShootEnemy : MonoBehaviour {
	public float minMove;
	public float maxMove;
	public enum state {Moving, Shooting};
	public float moveTimeLeft;
	public int shotsLeft;
	public float shotCooldown;
	public state aiState;
	public Vector3 movePoint;
	public GameObject bullet;
	NavMeshAgent agent;
	// Use this for initialization
	void Start () {
		minMove = GameObject.Find ("Spawner").GetComponent<EnemySpawner> ().minSpawn;
		maxMove = GameObject.Find ("Spawner").GetComponent<EnemySpawner> ().maxSpawn;
		//this.renderer.material.color = Color.green;
		movePoint.x = Random.Range(minMove, maxMove);
		movePoint.z = Random.Range(minMove, maxMove);
		aiState = state.Moving;
		agent = this.GetComponent<NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (this.aiState == state.Moving) {
			if (moveTimeLeft <= 0 || ( (this.GetComponent<Transform>().position.x - movePoint.x < 0.1) && this.GetComponent<Transform>().position.z - movePoint.z < 0.1)) {
				//out of time - start shooting
				aiState = state.Shooting;
				moveTimeLeft = Random.Range (8f, 24f) ; 
				agent.Stop();
			} else {
				//still moving
				//this.GetComponent<Transform> ().position = Vector3.MoveTowards (this.GetComponent<Transform> ().position, movePoint, 5 * Time.deltaTime);
				agent.SetDestination(movePoint);
				moveTimeLeft -= Time.deltaTime;

			}
		}
		if(aiState == state.Shooting){
			this.transform.LookAt(GameObject.FindGameObjectWithTag("PlayerTank").transform.position);
			if(shotCooldown <= 0){
				shotCooldown = 0.5f;
				if(shotsLeft > 0){
					shotsLeft--;
					//Spawn bullet
					Vector3 targetPosition = GameObject.FindWithTag("PlayerTank").transform.position - transform.position;
					//Debug.Log("Shooting at" + targetPosition);
					targetPosition *= 60f;
					Vector3 position = targetPosition + GameObject.FindWithTag("PlayerTank").transform.position;
					Object spawnedBullet = Instantiate (bullet, this.GetComponent<Transform>().position, Quaternion.identity);
					((GameObject)spawnedBullet).tag="EnemyBullet";
					SphereCollider[] bulletColliders= ((GameObject)spawnedBullet).GetComponents<SphereCollider>();
					SphereCollider bulletCollider=null;
					BoxCollider[] enemyColliders = this.GetComponents<BoxCollider>();
					BoxCollider enemyCollider=null;
					foreach( var c in bulletColliders){
						if (!c.isTrigger)
							bulletCollider=c;
					}
					foreach( var c in enemyColliders){
						if (!c.isTrigger)
							enemyCollider=c;
					}
					//Physics.IgnoreCollision(bulletCollider,enemyCollider);
					((GameObject)spawnedBullet).GetComponent<MoveToPoint> ().target = position;
					Object.Destroy (spawnedBullet, 5f);
				} else {
					//Out of shots - start moving
					shotsLeft = Random.Range(5,5);
					aiState = state.Moving;
					movePoint.x = Random.Range(minMove, maxMove);
					movePoint.z = Random.Range(maxMove, minMove);
				}
			} else {
				//Shot cooldown hasn't expired yet - not yet time to shoot
				shotCooldown-=Time.deltaTime;
			}
		}
	}
}
