﻿using UnityEngine;
using System.Collections;

public class PowerupSpawner : MonoBehaviour {
	public float minSpawn;
	public float maxSpawn;
	public float cooldown = 5f;
	public float maxCooldown = 5f;
	public GameObject healthPickupAddedFromEditor;
	public GameObject machineGunPickupAddedFromEditor;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(cooldown <= 0) {
			float rand = Random.Range(0f, 1f);
			if (rand < 0.5f) {
				spawnHealthPickup();
			} else {
				spawnMachineGunPickup();
			}
			cooldown = maxCooldown;
		} else {
			cooldown -= Time.deltaTime;
		}
	}

	void spawnHealthPickup(){
		Vector3 spawnPosition = new Vector3 ();
		spawnPosition.x = Random.Range (minSpawn, maxSpawn);
		spawnPosition.z = Random.Range (minSpawn, maxSpawn);
		if (Physics.OverlapSphere (spawnPosition, 1f).Length > 0) {
			Debug.Log ("incorrect placement");
			spawnHealthPickup ();
			return;
		}
		Instantiate (healthPickupAddedFromEditor, spawnPosition, Quaternion.identity);
	}

 	void spawnMachineGunPickup(){
		Vector3 spawnPosition = new Vector3 ();
		spawnPosition.x = Random.Range (minSpawn, maxSpawn);
		spawnPosition.z = Random.Range (minSpawn, maxSpawn);
		if (Physics.OverlapSphere (spawnPosition, 1f).Length > 0) {
			Debug.Log ("incorrect placement");
			spawnMachineGunPickup();
			return;
		}
		Instantiate (machineGunPickupAddedFromEditor, spawnPosition, Quaternion.identity);
	}
}
