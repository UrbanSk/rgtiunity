﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class PlayerManager : MonoBehaviour {

	public Text scoreText;
	public GameObject scoreCounter;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void DestroyPlayer(){
		GameOver();
	}

	void GameOver(){
		Time.timeScale = 0f;
		//GameObject scoreCounter = GameObject.FindWithTag("ScoreCounter");
		int score = scoreCounter.GetComponent<ScoreCounter> ().score;
		string name = PlayerPrefs.GetString ("playerName");
		int place = scoreCounter.GetComponent<HighScoreManager> ().insertScore (score, name);
		scoreText.text = scoreCounter.GetComponent<HighScoreManager> ().HighScoresText ();
		scoreText.transform.parent.gameObject.SetActive (true);
		//GameObject.Destroy (this.gameObject);
	}
}
