﻿using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour {
	public Vector3 offset= new Vector3(0f,20f,-9.5f);
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.position = GameObject.FindWithTag ("PlayerTank").transform.position + offset;
		
	}
}
