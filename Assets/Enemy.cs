﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	public int health = 1;
	public ScoreCounter scoreCounter;
	public int pointValue = 10;
	// Use this for initialization
	void Start () {
		scoreCounter = GameObject.Find ("ScoreCounter").GetComponent<ScoreCounter> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (health <= 0) {
			Destroy(this.gameObject);
			scoreCounter.score += pointValue;
		}
	}

}
