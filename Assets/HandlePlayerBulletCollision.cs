﻿using UnityEngine;
using System.Collections;

public class HandlePlayerBulletCollision : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		//k
		//Just handling collision between player bullets and enemies

		if (this.gameObject.tag.Contains ("Enemy") && other.gameObject.tag == "PlayerBullet") {
			this.GetComponent<Enemy> ().health -= 10;
			Destroy (other.gameObject);
		}
	}
}

