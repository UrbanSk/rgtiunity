﻿using UnityEngine;
using System.Collections;

public class DeathTimer : MonoBehaviour {

	public float initialTimetValue= 10f;
	public float timer;
	// Use this for initialization
	void Start () {
		timer = initialTimetValue;
	}
	
	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime;
		if (timer <= 0) {

		}
	}
}
