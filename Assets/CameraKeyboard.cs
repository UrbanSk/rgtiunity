﻿using UnityEngine;
using System.Collections;

public class CameraKeyboard : MonoBehaviour {
	// Use this for initialization
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float x = Input.GetAxisRaw ("Horizontal");
		float y = Input.GetAxisRaw ("Vertical");
		this.GetComponent<MoveScriptTest> ().direction.x = x;
		this.GetComponent<MoveScriptTest> ().direction.y = y;
		
		if (Input.GetAxisRaw ("Fire1") > 0) {
			this.GetComponent<Shoot>().SpawnBullet();
		}
	}
}
