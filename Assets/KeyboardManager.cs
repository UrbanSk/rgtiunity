﻿using UnityEngine;
using System.Collections;

public class KeyboardManager : MonoBehaviour {
	public GameObject escapeMenu;
	public float tankHeightOffset = 0.95204f;
	public float cooldown = 0f;
	public float baseCooldown = 0.1f;
	private bool cameraStop = true;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		decreaseCooldown ();
		float x = Input.GetAxisRaw ("Horizontal");
		float z = Input.GetAxisRaw ("Vertical");
		this.GetComponent<MoveScriptTest> ().direction.x = x;
		this.GetComponent<MoveScriptTest> ().direction.z = z;
		if (Mathf.Abs(x) > 0 || Mathf.Abs(z) > 0) {
			this.GetComponent<Animator> ().SetBool ("IsMoving", true);
		} else {
			this.GetComponent<Animator> ().SetBool ("IsMoving", false);
		}


		if (Input.GetAxisRaw ("Fire1") > 0) {
			this.GetComponent<Shoot>().currentWeapon.shootFunction();
		}

		if (Input.GetAxisRaw ("Cancel") > 0) {
			Time.timeScale = 0;
			escapeMenu.SetActive(true);
		}

		if (Input.GetAxisRaw ("Jump") > 0) {
			if (canJump()  && cooldown<=0){
				this.GetComponent<Rigidbody>().AddForce(new Vector3(0,400,0));
				cooldown=baseCooldown;
			}
		}

		if (Input.GetAxis ("Camera") > 0 && cameraStop == true) {
			cameraStop = false;
			GameObject.Find("CameraManager").GetComponent<CameraSwitcher>().switchCamera();
		}
		if (Input.GetAxis ("Camera") == 0) {
			cameraStop = true;
		}
		

	}

	bool canJump(){
		RaycastHit hit = new RaycastHit ();
		float height = 0f;
		if (Physics.Raycast(transform.position,transform.TransformDirection (Vector3.down),out hit,Mathf.Infinity))
		{
			height = hit.distance;
			Debug.Log (height);
			if(Mathf.Abs(height - tankHeightOffset) < 0.001f) return true;
		}
		return false;
	}

	void decreaseCooldown(){
		if(cooldown > 0)
		cooldown -= Time.deltaTime;
	}
}
