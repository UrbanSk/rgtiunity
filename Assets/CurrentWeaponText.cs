﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CurrentWeaponText : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		string name = GameObject.FindWithTag("PlayerTank").GetComponent<Shoot>().currentWeapon.name;
		this.GetComponent<Text> ().text = name;
	}
}
