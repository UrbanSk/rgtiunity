﻿using UnityEngine;
using System.Collections;

public class TurretRotate : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.timeScale != 0) {
				Vector3 rotateTo = this.GetComponent<Shoot> ().mousePosition ();
				GameObject.FindWithTag ("TurretMount").transform.LookAt (rotateTo);
				//Debug.Log ("Rotated Towards: " + rotateTo);
				GameObject.FindWithTag ("TurretMount").transform.Rotate (new Vector3 (0, 90, 0));
		}
	}
}
