﻿using UnityEngine;
using System.Collections;

public class MachineGunPickup : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		if (this.gameObject.tag != "MachineGunPickup") {
			return;
		}
		if (other.tag == "TankBody") {
			Shoot shoot = GameObject.FindWithTag("PlayerTank").GetComponentInParent<Shoot>();
			if(shoot.currentWeapon == shoot.machineGun)
			{
				shoot.currentWeapon.currAmmo = shoot.machineGun.maxAmmo;
			} 
			else 
			{
				shoot.currentWeapon = shoot.machineGun;
				shoot.currentWeapon.currAmmo = shoot.machineGun.maxAmmo;
				shoot.shootCooldown = 0;
				shoot.canShoot = true;
			}
			
			Destroy(this.gameObject);
		}
	}
}
